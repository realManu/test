import React, { Component } from 'react';
import { AutoComplete, Col, Form, Input } from 'antd';
import { StyleSheet, css } from 'aphrodite';
import PropTypes from 'prop-types';
import LodashService from '../services/LodashService';

const styles = StyleSheet.create({
  teamInputField: {
    width: '250px',
  },
});

const FormItem = Form.Item;
const Option = AutoComplete.Option;

class TeamSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchResults: [],
      autoCompleteInput: '',
    };

    this.handleSearch = this.handleSearch.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }

  getSearchOptions() {
    const { searchResults } = this.state;
    const { chosenTeams, form } = this.props;
    console.log(form.getFieldsValue(['teamSearch']).teamSearch);
    return [
      ...searchResults
        .filter(team => !chosenTeams.includes(team))
        .map(team => <Option key={team.teamId}>{team.teamName}</Option>),
      <Option key={0}>{form.getFieldsValue(['teamSearch']).teamSearch}</Option>,
    ];
  }

  handleSearch(value) {
    console.log('search');
    const { allTeamsList } = this.props;
    this.setState({
      searchResults: allTeamsList
        .filter(team => team.teamName.toLowerCase().includes(value.toLowerCase())),
    }, () => console.log(this.state));
  }

  handleSelect(teamId) {
    console.log('select');
    const teamName = this.props.form.getFieldsValue(['teamSearch']).teamSearch;
    this.props.handleSelect(teamId, teamName);
    this.handleSearch('');
    setTimeout(() => this.props.form.resetFields(), 10);
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const searchOptions = this.getSearchOptions();
    console.log('Render teamsearch');
    console.log('SearchResults', this.state.searchResults);
    return (
      <Form>
        <FormItem label="Add Team">
          {getFieldDecorator('teamSearch')(<AutoComplete
            className={css(styles.teamInputField)}
            dataSource={searchOptions}
            onSelect={this.handleSelect}
            onSearch={this.handleSearch}
            placeholder="Search"
          />)}
        </FormItem>
      </Form>
    );
  }
}

TeamSearch.propTypes = {
  form: PropTypes.object.isRequired,
  allTeamsList: PropTypes.array.isRequired,
  handleSelect: PropTypes.func.isRequired,
  chosenTeams: PropTypes.arrayOf(PropTypes.shape({
    teamId: PropTypes.number.isRequired,
    teamName: PropTypes.string.isRequired,
    crestUrl: PropTypes.string,
  })).isRequired,
};

export default Form.create()(TeamSearch);
