import React from 'react';
import { List, Avatar } from 'antd';
import { StyleSheet, css } from 'aphrodite';

const styles = StyleSheet.create({
  crest: {
    height: '5em',
    width: '5em',
  },
});

const PlayerPool = ({ players }) => (
  <div>
    <h3>Players</h3>
    <List
      itemLayout="vertical"
      dataSource={players}
      renderItem={singlePlayer => (
        <List.Item>
          <List.Item.Meta
            title={singlePlayer.playerName}
            avatar={<Avatar icon="user" />}
          />
          {singlePlayer.team && <p key="team"><img key="crest" className={css(styles.crest)} src={singlePlayer.team.crestUrl} alt="No crest" /> {singlePlayer.team.teamName} </p>}
        </List.Item>
      )}
    />
  </div>
);

export default PlayerPool;
