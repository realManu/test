import _ from 'lodash';

class LodashService {
  static removeDuplicatesTeamArray(teams) {
    return _.uniqBy(teams, 'teamId');
  }
}

export default LodashService;