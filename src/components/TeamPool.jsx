import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, css } from 'aphrodite';

const styles = StyleSheet.create({
  crest: {
    height: '5em',
    width: '5em',
    marginRight: '1em',
  },
  tr: {
    marginTop: '1.5em',
  },
});

const TeamPool = ({ teams }) => {
  const teamList = teams.map(t => (
    <tr className={css(styles.tr)} key={t.teamId}>
      <td><img className={css(styles.crest)} src={t.crestUrl} alt="" /></td>
      <td>{t.teamName}</td>
    </tr>
  ));

  console.log('Teamview teams', teams);
  return (
    <div>
      <h3>Teams</h3>
      <table>
        <tbody>
          {teamList}
        </tbody>
      </table>
    </div>
  );
};

TeamPool.propTypes = {
  teams: PropTypes.arrayOf(PropTypes.shape({
    teamId: PropTypes.number.isRequired,
    teamName: PropTypes.string.isRequired,
  })),
};
export default TeamPool;
