import urljoin from 'url-join';
import { API_KEY } from '../config';

const capiBaseUrl = 'http://api.football-data.org/v2/';

const apiGetOptions = {
  method: 'GET',
  // credentials: 'same-origin',
  headers: {
    'X-Auth-Token': API_KEY,
    'X-Response-Control': 'minified',
    // Accept: 'application/json',
    // 'Content-Type': 'application/json',
  },
};

class FootballDataApi {
  static fetchCompetitions() {
    try {
      return fetch(urljoin(apiBaseUrl, 'competitions'), apiGetOptions)
        .then(response => response.json());
    } catch (e) {
      console.log('Fetch Error', e);
      return null;
    }
  }

  static fetchTeamsInCompetition(competitionId) {
    try {
      return fetch(urljoin(apiBaseUrl, 'competitions', `${competitionId}`, 'teams'), apiGetOptions)
        .then(response => response.json());
    } catch (e) {
      console.log('Fetch Error', e);
      return null;
    }
  }
}

export default FootballDataApi;
