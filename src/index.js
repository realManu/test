import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import Fiddle from './fiddle';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
