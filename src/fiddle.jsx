import React, { Component} from 'react';
import { AutoComplete } from 'antd';


class Fiddle extends Component {
  render() {
    localStorage.setItem('fiddleTeams', ['team1', 'team2']);
    const teams = localStorage.getItem('fiddleTeams');
    console.log(localStorage);
    return (
      <div>

        <AutoComplete
          dataSource={['one', 'two']}
        />
      </div>
    )
  }
}

export default Fiddle;
