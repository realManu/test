import React, { Component } from 'react';
import { Button, Col, Input, Row, Form, message } from 'antd';
import { StyleSheet, css } from 'aphrodite';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import TeamPool from './TeamPool';
import FootballDataApi from '../services/footballDataApi';
import TeamSearch from './TeamSearch';
import PlayerPool from './PlayerPool';


const styles = StyleSheet.create({
  page: {
    padding: '2em 2em 2em 8em',
  },
  playerInputField: {
    width: '200px',
  },
});

const FormItem = Form.Item;
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allTeams: {},
      chosenTeams: [],
      players: [],
    };

    this.handleSelect = this.handleSelect.bind(this);
    this.handleAddPlayer = this.handleAddPlayer.bind(this);
    this.drawTeams = this.drawTeams.bind(this);
  }

  componentDidMount() {
    const storedTeams = JSON.parse(localStorage.getItem('allTeams'));
    if (storedTeams) {
      console.log('Found teams in cache');
      this.setState({ allTeams: storedTeams });
    } else {
      console.log('Fetching teams from server');
      // Fetch all available teams in all available league
      const teamFetchPromises = []; // One Promise for each league
      Promise.resolve(FootballDataApi.fetchCompetitions())
        .then(competitions => competitions.forEach((competition) => {
          teamFetchPromises.push(FootballDataApi.fetchTeamsInCompetition(competition.id));
        }))
        .then(() => Promise.all(teamFetchPromises)
          .then((leagues) => {
            const allTeams = leagues.reduce((teamCollection, singleLeague) => {
              const teamsInSingleLeague = singleLeague.teams.reduce((teamData, singleTeam) => ({
                ...teamData,
                [singleTeam.id]: {
                  teamName: singleTeam.name,
                  teamId: singleTeam.id,
                  crestUrl: singleTeam.crestUrl,
                },
              }), {});
              return {
                ...teamCollection,
                ...teamsInSingleLeague,
              };
            }, {});

            localStorage.setItem('allTeams', JSON.stringify(allTeams));

            this.setState({
              allTeams,
            });
          }));
    }
  }

  handleSelect(teamId, teamName) {
    console.log('App handleselect');
    this.setState(prevState => ({
      chosenTeams: [
        ...prevState.chosenTeams,
        Object.prototype.hasOwnProperty.call(this.state.allTeams, teamId)
        ? prevState.allTeams[teamId]
        : { teamId: 0, teamName, crestUrl: null },
      ],
    }), () => console.log('Added new team, current teams', this.state.chosenTeams));
  }

  handleDelete(teamId) {
    // TODO
  }

  handleAddPlayer(evt) {
    const playerName = evt.target.value;

    this.setState(prevState => ({
      players: [
        ...prevState.players,
        { playerName, team: null },
      ],
    }), () => console.log('New player', this.state.players));

    document.getElementById('playerInput').value = '';
  }

  drawTeams() {
    const { players, chosenTeams } = this.state;
    const remainingTeams = [...chosenTeams];

    if (chosenTeams.length === 0) {
      message.error('Add teams to pool before drawing');
    } else if (players.length === 0) {
      message.error('Add players before drawing');
    } else {
      this.setState(prevState => ({
        players: prevState.players.map(singlePlayer => ({
          ...singlePlayer,
          team: remainingTeams.splice(Math.floor(Math.random() * remainingTeams.length), 1)[0],
        })),
      }));
    }
  }

  render() {
    const { allTeams, players, chosenTeams } = this.state;

    console.log('ALLTEAMS', allTeams);
    return (
      <div className={css(styles.page)}>
        <header>
          <h1>FIFA random Team Picker</h1>
        </header>
        <Row>
          <Col span={12}>
            <FormItem label="Add Player">
              <Input
                className={css(styles.playerInputField)}
                id="playerInput"
                onPressEnter={this.handleAddPlayer}
              />
            </FormItem>
            <PlayerPool players={players} />
            <Button
              onClick={this.drawTeams}
            >
              Draw teams
            </Button>
          </Col>
          <Col span={12}>
            <TeamSearch chosenTeams={chosenTeams} allTeamsList={Object.values(allTeams)} handleSelect={this.handleSelect} />
            <TeamPool teams={chosenTeams} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default App;
